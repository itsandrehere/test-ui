import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { retry, catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getClient(): Observable<any> {
    return this.http.get<any>("https://jsonplaceholder.typicode.com/users");
  }

  getCountry(): Observable<any> {
    return this.http.get<any>("https://restcountries.eu/rest/v2/region/americas");
  }

  getProducts(): Observable<any> {
    return this.http.get<any>("http://fakerestapi.azurewebsites.net/api/books");
  }
}
