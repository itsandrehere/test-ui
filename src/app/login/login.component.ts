import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { ApiService } from "../services/api.service";
import { TdDialogService } from "@covalent/core/dialogs";
import { FormControl, Validators, FormGroup } from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  clients: any;
  countries: any;
  productos: any;
  countrySelect: any;
  currencie: any;
  total: any = 0;

  details = [{ name: "", quantity: "", price: "", subtotal: "" }];
  Cliente: any;


  constructor(
    private Api: ApiService,
    private _dialogService: TdDialogService,
    private _viewContainerRef: ViewContainerRef
  ) {
    this.Cliente = new FormGroup({
      name: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
    this.getClientes();
    this.getPaises();
    this.getProductos();
  }

  getClientes() {
    this.Api.getClient().subscribe(res => {
      this.clients = res;
    });
  }
  getPaises() {
    this.Api.getCountry().subscribe(res => {
      this.countries = res;
    });
  }
  getProductos() {
    this.Api.getProducts().subscribe(res => {
      this.productos = res;
    });
  }

  getCurrency(value) {
    let data = JSON.parse(value);
    this.currencie = data.currencies[0].code;
  }

  Subtotal(item) {
    if (item.price && item.quantity) {
      item.subtotal = item.price * item.quantity;
    }
  }

  totals(){

    let totals = this.details.reduce((
      acc,
      obj,
    ) => acc + (parseInt(obj.quantity) * parseInt(obj.price)),
    0);
    if(totals){
      this.total = totals;
    }
  }

  message(msg) {
    this._dialogService.openAlert({
      message: msg,
      disableClose: false, // defaults to false
      viewContainerRef: this._viewContainerRef, //OPTIONAL
      title: "Alerta", //OPTIONAL, hides if not provided
      closeButton: "Cerrar", //OPTIONAL, defaults to 'CLOSE'
      width: "400px" //OPTIONAL, defaults to 400px
    });
  }
  addDetails() {
    this.details.push({ name: "", quantity: "", price: "", subtotal: "" });
  }
  removeDetails(i) {
    this.details.splice(i, 1);
  }

  resetCampos(){
    this.Cliente.controls['name'].reset();
    this.countrySelect = null;
    this.currencie = null;
    this.details = [];
    this.total = 0;
  }
  save() {
    console.log(this.details);
    if(this.details.length > 0){
      if(this.total != 0){
        this.message("Guardado exitoso!");
        this.resetCampos();
        return;
      }
    }
    this.message("Complete un detalle de venta para poder guardar.")
  }
}
