import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CovalentDialogsModule } from '@covalent/core/dialogs';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CovalentDialogsModule
  ],
  exports:[
    CovalentDialogsModule
  ]
})
export class ExternModule { }
